//
//  WebViewController.swift
//  Belsar
//
//  Created by Mathieu De Haeck on 05/10/14.
//  Copyright (c) 2014 Kandesign. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {

    @IBOutlet var webView: UIWebView!
    @IBOutlet var toolBar: UIToolbar!
    
    //var URLPath = "http://google.com"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //loadAdressURL("http://www.kadanza.com", opensInWebView: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadAdressURL(URLPath:NSString, opensInWebView:Bool) {
        let requestURL = NSURL(string: URLPath)
        let request = NSURLRequest(URL: requestURL)
        
        if (opensInWebView == true) {
            webView.loadRequest(request)
        } else {
            println("open url in safari")
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

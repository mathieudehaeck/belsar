//
//  AppDelegate.swift
//  Belsar
//
//  Created by Mathieu De Haeck on 03/10/14.
//  Copyright (c) 2014 Kandesign. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UINavigationControllerDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        //var tabBarController = UITabBarController
//        let rootVC: ViewController = ViewController(nibName: nil, bundle: nil)
//        self.window!.rootViewController = rootVC
        
        //let tabBarController:UITabBarController = UITabBarController.self(<#NSObject#>)
        
        // Change statusbar color
        application.setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
        
        // Tabbar
        if self.window != nil {
            //set tab bar to be light gray
            //(self.window!.rootViewController as UITabBarController).tabBar.backgroundImage = UIImage(named: "tab.png")
            //(self.window!.rootViewController as UITabBarController).tabBar.translucent = false
            //setting delegate for styling
            
            // self.yourTabBarController is an IBOutlet to your UITabBar controller
            let tabBar = (self.window!.rootViewController as UITabBarController).tabBar
            
            tabBar.translucent = false
            tabBar.barTintColor = UIColor.darkGrayColor()
            tabBar.tintColor = UIColor.whiteColor()
            tabBar.selectedImageTintColor = UIColor.orangeColor()
            
            //tabBar.selectedItem?.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.redColor()], forState: UIControlState.Normal)

            
            // UITabBar Items are an array in order (0 is the first item)
            let tabItems = tabBar.items as [UITabBarItem]
            
            func setTabItem(itemNumber:Int, itemName:String) {
                tabItems[itemNumber].title = itemName
                var itemNameInLowerCase = itemName.lowercaseString
                tabItems[itemNumber].image = UIImage(named: itemNameInLowerCase)
                tabItems[itemNumber].selectedImage = UIImage(named: "\(itemNameInLowerCase)" + "_filled")
            }
            
            setTabItem(0, "Ports")
            setTabItem(1, "VBZR")
            setTabItem(2, "Safety")
            setTabItem(3, "Passport")
            setTabItem(4, "Donate")
            
            (self.window!.rootViewController as UITabBarController).moreNavigationController.delegate = self
        }
        
        return true
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}


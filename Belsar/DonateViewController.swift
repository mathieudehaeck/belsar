//
//  DonateViewController.swift
//  Belsar
//
//  Created by Mathieu De Haeck on 04/10/14.
//  Copyright (c) 2014 Kandesign. All rights reserved.
//

import UIKit

class DonateViewController: UIViewController, UINavigationBarDelegate {
    
    @IBOutlet var navBar : UINavigationBar!
    @IBOutlet var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //navbar style
        //self.navBar.setBackgroundImage(UIImage(named: "bar.png"), forBarMetrics: UIBarMetrics.Default)
        self.navBar.barTintColor = UIColor.darkGrayColor()
        self.navBar.tintColor = UIColor.whiteColor()
        self.navBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        self.navBar.topItem?.title = "Donate"
        
        //text
        self.textView.text =
        "Being a non-profit organization Belsar (VBZR) depends entirely from outside donations and non-life saving interventions. \n\n" +
        "If you wish to support us you can do so by one of these formula’s:\n\n" +
        "1. Rescuer ashore (€10.00)\n\n" +
        "2. Full supporting member (€70.00)\n\n" +
        "With only a few clicks you can support us from within this app."
        
        //donate
        //loadAdressURL("http://www.kadanza.com", opensInWebView: true)
    }
    
    func positionForBar(bar: UIBarPositioning!) -> UIBarPosition  {
        return UIBarPosition.TopAttached
    }
}
